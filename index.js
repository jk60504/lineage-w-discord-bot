const node_fs = require('node:fs');
const node_path = require('node:path');
// Require the necessary discord.js classes
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

// global variable
const commands_path = node_path.join(__dirname, 'commands');

// Create a new client instance
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

// Register slash command to client
registerSlashCommand();

// When the client is ready, run this code (only once)
// We use 'c' for the event parameter to keep it separate from the already defined 'client'
client.once(Events.ClientReady, c => {
	console.log(`Ready! Logged in as ${c.user.tag}`);
});

client.on(Events.InteractionCreate, async interaction => {
    if (!interaction.isChatInputCommand()) return;

	console.log(interaction);

    var event_cmd = interaction.client.commands.get(interaction.commandName);
	if (!event_cmd) {
		console.error(`No command matching ${interaction.commandName} was found.`);
		return;
	}

	try {
		await event_cmd.execute(interaction);
	} catch (error) {
		console.error(error);
		if (interaction.replied || interaction.deferred) {
			await interaction.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
		} else {
			await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
		}
	}
});

// Log in to Discord with your client's token
client.login(token);

// 
function registerSlashCommand() {
    client.commands = new Collection();
    var cmd_file, cmd_module;
    var cmd_files = node_fs.readdirSync(commands_path).filter(file => file.endsWith('.js'));

    for (cmd_file of cmd_files) {
        // required command module
        cmd_module = require(node_path.join(commands_path, cmd_file));

	    // Set a new item in the Collection with the key as the command name and the value as the exported module
	    if (!('data' in cmd_module) || !('execute' in cmd_module)) {
            console.log(`[WARNING] ${cmd_file} is missing a required "data" or "execute" property.`);
            continue;
        }

        client.commands.set(cmd_module.data.name, cmd_module);
    }
}